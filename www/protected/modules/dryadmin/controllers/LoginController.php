<?php
class LoginController extends Controller
{
	public $defaultAction = 'login';
	public $layout = 'login';
	
	public function actionLogin()
	{
		$model = new LoginForm();
		
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			if ($model->validate()) {
				Yii::app()->user->login($model->identity);
				
				if (Yii::app()->user->returnUrl != '/index.php')
					$this->redirect(Yii::app()->user->returnUrl);
				else
					$this->redirect($this->createUrl('section/index', array('section' => 'page')));
			}
		}
		
		$this->render('login', array('model' => $model));
	}
}