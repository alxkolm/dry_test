<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />

	<!-- 
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin/reset.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin/main.css" />
	
	<?php echo CHtml::cssFile('/js/admin/jquery-ui/css/Aristo/jquery-ui-1.8.7.custom.css')?>
	<?php echo CHtml::cssFile('/js/admin/elrte/css/elrte.min.css')?>
	<?php echo CHtml::cssFile('/js/admin/elfinder/css/elfinder.css')?>
	
	<?php echo CHtml::scriptFile('/js/admin/jquery-1.5.2.min.js'); ?>
	<?php echo CHtml::scriptFile('/js/admin/jquery-ui/js/jquery-ui-1.8.11.custom.min.js'); ?>
	<?php echo CHtml::scriptFile('/js/admin/jquery-ui/js/i18n/jquery.ui.datepicker-ru.js'); ?>
	<?php echo CHtml::scriptFile('/js/admin/elrte/js/elrte.min.js'); ?>
	<?php echo CHtml::scriptFile('/js/admin/elrte/js/i18n/elrte.ru.js'); ?>
	
	<?php echo CHtml::scriptFile('/js/admin/elfinder/js/elfinder.min.js'); ?>
	<?php echo CHtml::scriptFile('/js/admin/elfinder/js/i18n/elfinder.ru.js'); ?>
	
	
	<?php echo CHtml::script("$.datepicker.setDefaults($.datepicker.regional['ru']);")?>
	 -->
	
	<?php Yii::app()->clientScript->registerCssFile($this->module->assetsUrl.'/twitter-bootstrap/bootstrap.css'); ?>
	<?php Yii::app()->clientScript->registerCssFile($this->module->assetsUrl.'/iconic/iconic.css'); ?>
	<?php Yii::app()->clientScript->registerCssFile($this->module->assetsUrl.'/admin-style.css'); ?>
	

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
	<?php echo $content; ?>
</body>
</html>