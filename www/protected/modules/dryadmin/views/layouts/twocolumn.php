<?php $this->beginContent('/layouts/body'); ?>
<div class="topbar">
	<div class="topbar-inner">
		<div class="container-fluid">
			<a class="brand" href="<?php echo $this->createUrl('section/index', array('section' => 'page')); ?>">DryCMS</a>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="sidebar">
		<div class="well">
			<ul>
				<?php foreach ($this->module->structure as $sectionId => $section):?>
					<?php if (!isset($section['master'])): ?>
					<li><a href="<?php echo $this->createUrl('index', array('section' => $sectionId)); ?>"><?php echo $section['label']; ?></a></li>
					<?php endif; ?>
				<?php endforeach;?>
			</ul>
		</div>
	</div>
	<div class="content">
		<?php $this->widget('DBreadcrumbs', array('links' => $this->breadcrumbs, 'homeLink' => FALSE)); ?>
		<?php echo $content; ?>
	</div>
</div>
<?php $this->endContent();?>