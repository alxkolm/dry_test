<?php

/**
 * Twitter Bootstrap Breadcrumbs
 * Breadcrumbs marup for Twitter Bootstrap
 * @author alx
 *
 */
class DBreadcrumbs extends CBreadcrumbs
{
	/**
	* @var string the tag name for the breadcrumbs container tag. Defaults to 'ul'.
	*/
	public $tagName='ul';
	
	/**
	* @var array the HTML attributes for the breadcrumbs container tag.
	*/
	public $htmlOptions=array('class'=>'breadcrumb');
	
	/**
	* @var string the separator between links in the breadcrumbs. Defaults to ' &raquo; '.
	*/
	public $separator=' / ';
	
	/**
	 * Renders the content of the portlet.
	 */
	public function run()
	{
		if(empty($this->links))
			return;

		echo CHtml::openTag($this->tagName,$this->htmlOptions)."\n";
		$links=array();
		if($this->homeLink===null)
			$links[]=CHtml::link(Yii::t('zii','Home'),Yii::app()->homeUrl);
		else if($this->homeLink!==false)
			$links[]=$this->homeLink;
		foreach($this->links as $label=>$url)
		{
			if(is_string($label) && !empty($url))
				$links[]=CHtml::openTag('li').
						 CHtml::link($this->encodeLabel ? CHtml::encode($label) : $label, $url).
						 CHtml::openTag('span', array('class' => 'divider')).$this->separator.CHtml::closeTag('span').
						 CHtml::closeTag('li');
			elseif(is_string($label))
				$links[]=CHtml::openTag('li').
						 ($this->encodeLabel ? CHtml::encode($label) : $label).
						 CHtml::openTag('span', array('class' => 'divider')).$this->separator.CHtml::closeTag('span').
						 CHtml::closeTag('li');
			else
				$links[]=CHtml::openTag('li').
						 '<span>'.($this->encodeLabel ? CHtml::encode($url) : $url).'</span>'.
						 CHtml::closeTag('li');
		}
		echo implode('',$links);
		echo CHtml::closeTag($this->tagName);
	}
}