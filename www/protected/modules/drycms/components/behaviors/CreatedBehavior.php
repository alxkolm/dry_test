<?php
class CreatedBehavior extends CActiveRecordBehavior
{
	public $field_created = 'created';

	public function beforeSave()
	{
		if ($this->getOwner()->isNewRecord) {
			$this->getOwner()->{$this->field_created} = date('Y-m-d H:i:s');
		}
	}
}