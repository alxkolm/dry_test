<?php
class TreeBehaviorTest extends CDbTestCase
{
	public $fixtures = array(
		'pages' => 'Page'
	);
	
	public function testRoot()
	{
		$p = new Page();
		$pages = $p->root()->findAll();
		
		// Количество элементов с prent_id = 0
		$c = 0;
		foreach ($this->pages as $p) {
			if ($p['parent_id'] == 0) $c++;
		}
		
		$this->assertEquals($c, count($pages));
	}
	
	public function testChildOf()
	{
		$p = new Page();
		$pages = $p->childOf(3)->findAll();
		
		// Количество элементов с prent_id = 3
		$c = 0;
		foreach ($this->pages as $p) {
			if ($p['parent_id'] == 3) $c++;
		}
		
		$this->assertEquals($c, count($pages));
	}
	
	public function testParents()
	{
		$p = Page::model()->findByPk(9);
		$parents = $p->parents;
		$this->assertEquals(2, count($parents));
		
		$p = Page::model()->findByPk(5);
		$parents = $p->parents;
		$this->assertEquals(1, count($parents));
		
		$p = Page::model()->findByPk(3);
		$parents = $p->parents;
		$this->assertEquals(0, count($parents));
	}
	
	
}