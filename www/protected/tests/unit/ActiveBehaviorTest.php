<?php
class ActiveBehaviorTest extends CDbTestCase
{
	public $fixtures = array(
		'pages' => 'Page'
	);
	
	public function testSave()
	{
		$page = new Page();
		
		$page->title = 'New page';
		$page->deleted = 0;
		$page->url = 'new';
		$page->active = 0;
		
		
		$this->assertEquals(TRUE, $page->save());
		
		$p = Page::model()->findByPk($page->id);
		
		$this->assertEquals(0, $p->active);
	}
	
	public function testIsActive()
	{
		$page = new Page();
		
		$page->title = 'New page';
		$page->deleted = 0;
		$page->url = 'new';
		$page->active = 0;
		
		$page->save();
		
		$this->assertEquals(FALSE, $page->isActive());
		
		$page = Page::model()->findByPk($page->id);
		$this->assertEquals(FALSE, $page->isActive());
		
		$page->active = 1;
		$page->save();
		$this->assertEquals(TRUE, $page->isActive());
		
		$page = Page::model()->findByPk($page->id);
		$this->assertEquals(TRUE, $page->isActive());
		
	}
	
	public function testIsActiveProperty()
	{
		$page = new Page();
		
		$page->title = 'New page';
		$page->deleted = 0;
		$page->url = 'new';
		$page->active = 0;
		
		$page->save();
		
		$this->assertEquals(FALSE, $page->isActive);
		
		$page = Page::model()->findByPk($page->id);
		$this->assertEquals(FALSE, $page->isActive);
		
		$page->active = 1;
		$page->save();
		$this->assertEquals(TRUE, $page->isActive);
		
		$page = Page::model()->findByPk($page->id);
		$this->assertEquals(TRUE, $page->isActive);
		
	}
	
	public function testActive()
	{
		$pages = Page::model()->active()->findAll();
		
		// Подсчитываем активные страницы
		$activeCount = 0;
		foreach ($this->pages as $p) {
			if ($p['active'] == 1) $activeCount++;
		}
		
		
		$this->assertEquals($activeCount, count($pages)); 
	}
	
	public function testNotActive()
	{
		$pages = Page::model()->notActive()->findAll();
		
		// Подсчитываем активные страницы
		$notActiveCount = 0;
		foreach ($this->pages as $p) {
			if ($p['active'] == 0) $notActiveCount++;
		}
		
		$this->assertEquals($notActiveCount, count($pages)); 
	}
	
	public function testActivate()
	{
		$page = new Page();
		
		$page->title = 'New page';
		$page->deleted = 0;
		$page->url = 'new';
		$page->active = 0;
		
		$page->save();
		$page = Page::model()->findByPk($page->id);
		$page->activate()->save();
		$page = Page::model()->findByPk($page->id);
		$this->assertEquals(TRUE, $page->isActive());
		
	}
	
	public function testDeActivate()
	{
		$page = new Page();
		
		$page->title = 'New page';
		$page->deleted = 0;
		$page->url = 'new';
		$page->active = 1;
		
		$page->save();
		$page = Page::model()->findByPk($page->id);
		$page->deActivate()->save();
		$page = Page::model()->findByPk($page->id);
		$this->assertEquals(FALSE, $page->isActive());
		
	}
}